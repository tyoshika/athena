/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/// @author Tadej Novak
/// @author Andrei Gaponenko <agaponenko@lbl.gov>, 2006-2009

#include <Identifier/Identifier.h>
#include <Identifier/IdentifierHash.h>

#include "IDC_OverlayCommon.h"


template <class IDC_Container>
StatusCode IDC_OverlayBase::overlayContainer(const IDC_Container *bkgContainer,
                                             const IDC_Container *signalContainer,
                                             IDC_Container *outputContainer) const
{
  ATH_MSG_DEBUG("overlayContainer<>() begin");

  typedef typename IDC_Container::base_value_type Collection;

  // Get all the hashes for the signal container
  const std::vector<IdentifierHash> signalHashes = signalContainer->GetAllCurrentHashes();

  // There are some use cases where background is empty
  if (!bkgContainer) {
    // Only loop through the signal collections and copy them over
    for (const IdentifierHash &hashId : signalHashes) {
      // Copy the signal collection
      std::unique_ptr<Collection> signalCollection = Overlay::copyCollection(hashId, signalContainer->indexFindPtr(hashId));

      if (outputContainer->addCollection(signalCollection.get(), hashId).isFailure()) {
        ATH_MSG_ERROR("Adding signal Collection with hashId " << hashId << " failed");
        return StatusCode::FAILURE;
      } else {
        signalCollection.release();
      }
    }

    return StatusCode::SUCCESS;
  }

  // Get all the hashes for the background container
  const std::vector<IdentifierHash> bkgHashes = bkgContainer->GetAllCurrentHashes();
  
  // The MC signal container should typically be smaller than bkgContainer,
  // because the latter contains all the noise, minimum bias and pile up.
  // Thus we firstly iterate over signal hashes and store them in a map.
  std::map<IdentifierHash, bool> overlapMap;
  for (const IdentifierHash &hashId : signalHashes) {
    overlapMap.emplace(hashId, false);
  }

  // Now loop through the background hashes and copy unique ones over
  for (const IdentifierHash &hashId : bkgHashes) {
    auto search = overlapMap.find(hashId);
    if (search == overlapMap.end()) {
      // Copy the background collection
      std::unique_ptr<Collection> bkgCollection = Overlay::copyCollection(hashId, bkgContainer->indexFindPtr(hashId));

      if (outputContainer->addCollection(bkgCollection.get(), hashId).isFailure()) {
        ATH_MSG_ERROR("Adding background Collection with hashId " << hashId << " failed");
        return StatusCode::FAILURE;
      } else {
        bkgCollection.release();
      }
    } else {
      // Flip the overlap flag
      search->second = true;
    }
  }

  // Finally loop through the map and process the signal and overlay if
  // necessary
  for (const auto &[hashId, overlap] : overlapMap) {
    // Copy the signal collection
    std::unique_ptr<Collection> signalCollection = Overlay::copyCollection(hashId, signalContainer->indexFindPtr(hashId));

    if (overlap) { // Do overlay
      // Create the output collection, only works for Inner Detector
      auto outputCollection = std::make_unique<Collection>(hashId);
      outputCollection->setIdentifier(signalCollection->identify());
      // Copy the background collection
      std::unique_ptr<Collection> bkgCollection = Overlay::copyCollection(hashId, bkgContainer->indexFindPtr(hashId));

      // Merge collections
      Overlay::mergeCollections(bkgCollection.get(), signalCollection.get(), outputCollection.get(), this);

      if (outputContainer->addCollection(outputCollection.get(), hashId).isFailure()) {
        ATH_MSG_ERROR("Adding overlaid Collection with hashId " << hashId << " failed");
        return StatusCode::FAILURE;
      } else {
        outputCollection.release();
      }
    } else { // Only write signal out
      if (outputContainer->addCollection(signalCollection.get(), hashId).isFailure()) {
        ATH_MSG_ERROR("Adding signal Collection with hashId " << hashId << " failed");
        return StatusCode::FAILURE;
      } else {
        signalCollection.release();
      }
    }
  }

  return StatusCode::SUCCESS;
}
